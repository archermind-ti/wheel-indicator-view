package com.dlazaro66.sampleapp;

import com.dlazaro66.sampleapp.slice.MainSlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class MainAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(MainSlice.class.getName());
    }
}
