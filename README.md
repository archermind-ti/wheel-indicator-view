# WheelIndicatorView

#### 介绍

一个类似’google fit‘的OHOS活动指示器

#### 功能

1. 添加item到进度条
2. 播放动画


#### 演示

![image](./i2clg-dmpah.gif)

#### 集成

1.下载模块包关联使用
```
    dependencies {
        implementation fileTree(dir: 'libs', include: ['*.jar'])
        implementation project(':wheelindicatorview')
        testImplementation 'junit:junit:4.13'
    }
```
2.maven引用
```
    ...
    allprojects {
        repositories {
            mavenCentral()
        }
    }
    ...
     dependencies {
     ...
     implementation 'com.gitee.archermind-ti:WheelIndicatorView:1.0.1-beta'
     ...
    }
```

#### 使用说明
```
    wheelIndicatorView = (WheelIndicatorView) findComponentById(ResourceTable.Id_wheel_indicator_view);

        // dummy data
        float dailyKmsTarget = 4.0f; // 4.0Km is the user target, for example
        float totalKmsDone = 3.0f; // User has done 3 Km
        int percentageOfExerciseDone = (int) (totalKmsDone / dailyKmsTarget * 100); 

        wheelIndicatorView.setFilledPercent(percentageOfExerciseDone);

        WheelIndicatorItem bikeActivityIndicatorItem = new WheelIndicatorItem(1.8f, Color.getIntColor("#ff9000"));
        WheelIndicatorItem walkingActivityIndicatorItem = new WheelIndicatorItem(0.9f, Color.argb(255, 194, 30, 92));
        WheelIndicatorItem runningActivityIndicatorItem = new WheelIndicatorItem(0.3f, getColor(ResourceTable.Color_my_wonderful_blue_color));

        wheelIndicatorView.addWheelIndicatorItem(bikeActivityIndicatorItem);
        wheelIndicatorView.addWheelIndicatorItem(walkingActivityIndicatorItem);
        wheelIndicatorView.addWheelIndicatorItem(runningActivityIndicatorItem);

        button = (Button) findComponentById(ResourceTable.Id_start_anime);
        button.setClickedListener(component -> {
            wheelIndicatorView.startItemsAnimation(); // Animate!
        });
```

#### 编译说明

1. 将项目通过git clone 至本地
2. 使用DevEco Studio 打开该项目，然后等待Gradle 构建完成
3. 点击Run运行即可（真机运行可能需要配置签名）

#### 版本迭代

+ V1.0.1


#### 版权和许可信息
```
    Copyright 2015 David Lázaro
    
    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at
    
       http://www.apache.org/licenses/LICENSE-2.0
    
    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
```
