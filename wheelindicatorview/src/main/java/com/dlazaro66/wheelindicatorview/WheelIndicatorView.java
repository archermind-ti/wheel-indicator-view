/*
 * Copyright (C) 2015 David Lázaro Esparcia
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.dlazaro66.wheelindicatorview;

import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.render.Arc;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;
import ohos.agp.utils.Rect;
import ohos.agp.utils.RectFloat;
import ohos.app.Context;

import java.util.ArrayList;
import java.util.List;

public class WheelIndicatorView extends Component implements Component.DrawTask, Component.EstimateSizeListener {

    private final static int ANGLE_INIT_OFFSET = -90;
    private final static int DEFAULT_FILLED_PERCENT = 100;
    private final static int DEFAULT_ITEM_LINE_WIDTH = 25;
    public static final int ANIMATION_DURATION = 1200;
    public static final int INNER_BACKGROUND_CIRCLE_COLOR = Color.argb(255, 220, 220, 220); // Color for

    private Paint itemArcPaint;
    private Paint itemEndPointsPaint;
    private Paint innerBackgroundCirclePaint;
    private List<WheelIndicatorItem> wheelIndicatorItems;
    private int viewHeight;
    private int viewWidth;
    private int minDistViewSize;
    private int maxDistViewSize;
    private int traslationX;
    private int traslationY;
    private Rect wheelBoundsRectF;
    private Paint circleBackgroundPaint;
    private ArrayList<Float> wheelItemsAngles;     // calculated angle for each @WheelIndicatorItem
    private int filledPercent = 80;
    private int itemsLineWidth = 25;
    private AnimatorValue animation;
    private int tmpFilledPercent;

    public WheelIndicatorView(Context context, AttrSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    public WheelIndicatorView(Context context, AttrSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public WheelIndicatorView(Context context) {
        super(context);
        init(null);
    }

    public void setWheelIndicatorItems(List<WheelIndicatorItem> wheelIndicatorItems) {
        if (wheelIndicatorItems == null)
            throw new IllegalArgumentException("wheelIndicatorItems cannot be null");
        this.wheelIndicatorItems = wheelIndicatorItems;
        recalculateItemsAngles();
        invalidate();
    }

    public void setFilledPercent(int filledPercent) {
        tmpFilledPercent = filledPercent;
        if (filledPercent < 0)
            this.filledPercent = 0;
        else if (filledPercent > 100)
            this.filledPercent = 100;
        else
            this.filledPercent = filledPercent;
        invalidate();
    }

    public int getFilledPercent() {
        return filledPercent;
    }

    public void setItemsLineWidth(int itemLineWidth) {
        if (itemLineWidth <= 0)
            throw new IllegalArgumentException("itemLineWidth must be greater than 0");
        this.itemsLineWidth = itemLineWidth;
        invalidate();
    }

    public void addWheelIndicatorItem(WheelIndicatorItem indicatorItem) {
        if (indicatorItem == null)
            throw new IllegalArgumentException("wheelIndicatorItems cannot be null");

        this.wheelIndicatorItems.add(indicatorItem);
        recalculateItemsAngles();
        invalidate();
    }

    public void notifyDataSetChanged() {
        recalculateItemsAngles();
        invalidate();
    }

    public void setBackgroundColor(Color color) {
        circleBackgroundPaint = new Paint();
        circleBackgroundPaint.setColor(color);
        invalidate();
    }

    private void init(AttrSet attrs) {
        animation = new AnimatorValue();
        setEstimateSizeListener(this);
        addDrawTask(this);
        if (attrs != null) {
            if (attrs.getAttr("itemsLineWidth").isPresent()) {
                int itemsLineWidth = attrs.getAttr("itemsLineWidth").get().getDimensionValue();
                setItemsLineWidth(itemsLineWidth);
            } else {
                setItemsLineWidth(DEFAULT_ITEM_LINE_WIDTH);
            }

            if (attrs.getAttr("filledPercent").isPresent()) {
                int filledPercent = attrs.getAttr("filledPercent").get().getIntegerValue();
                setFilledPercent(filledPercent);
            } else {
                setFilledPercent(DEFAULT_FILLED_PERCENT);
            }

            if (attrs.getAttr("backgroundColor").isPresent()) {
                Color bgColor = attrs.getAttr("backgroundColor").get().getColorValue();
                setBackgroundColor(bgColor);
            }
        }

        this.wheelIndicatorItems = new ArrayList<>();
        this.wheelItemsAngles = new ArrayList<>();

        itemArcPaint = new Paint();
        itemArcPaint.setStyle(Paint.Style.STROKE_STYLE);
        itemArcPaint.setStrokeWidth(itemsLineWidth * 2);
        itemArcPaint.setAntiAlias(true);

        innerBackgroundCirclePaint = new Paint();
        innerBackgroundCirclePaint.setColor(new Color(INNER_BACKGROUND_CIRCLE_COLOR));
        innerBackgroundCirclePaint.setStyle(Paint.Style.STROKE_STYLE);
        innerBackgroundCirclePaint.setStrokeWidth(itemsLineWidth * 2);
        innerBackgroundCirclePaint.setAntiAlias(true);

        itemEndPointsPaint = new Paint();
        itemEndPointsPaint.setAntiAlias(true);
    }

    private void recalculateItemsAngles() {
        wheelItemsAngles.clear();
        float total = 0;
        float angleAccumulated = 0;

        for (WheelIndicatorItem item : wheelIndicatorItems) {
            total += item.getWeight();
        }
        for (int i = 0; i < wheelIndicatorItems.size(); ++i) {
            float normalizedValue = wheelIndicatorItems.get(i).getWeight() / total;
            float angle = 360 * normalizedValue * filledPercent / 100;
            wheelItemsAngles.add(angle + angleAccumulated);
            angleAccumulated += angle;
        }
    }

    public void startItemsAnimation() {
        if (animation.isRunning()){
            animation.stop();
        }
        animation.setDuration(ANIMATION_DURATION);
        animation.setDelay(100);
        animation.setCurveType(Animator.CurveType.CUBIC_BEZIER_STANDARD);
        animation.setValueUpdateListener((animatorValue, v) -> {
            filledPercent = (int) (tmpFilledPercent * v);
            recalculateItemsAngles();
            invalidate();

        });
        animation.start();
    }

    @Override
    public boolean onEstimateSize(int w, int h) {
        int width = Component.EstimateSpec.getSize(w);
        int height = Component.EstimateSpec.getSize(h);
        if (width > height) {
            width = height;
        } else {
            height = width;
        }
        setEstimatedSize(
                Component.EstimateSpec.getChildSizeWithMode(width, width, Component.EstimateSpec.NOT_EXCEED),
                Component.EstimateSpec.getChildSizeWithMode(height, height, Component.EstimateSpec.NOT_EXCEED));
        this.viewHeight = getEstimatedHeight();
        this.viewWidth = getEstimatedWidth();
        this.minDistViewSize = Math.min(getEstimatedWidth(), getEstimatedHeight());
        this.maxDistViewSize = Math.max(getEstimatedWidth(), getEstimatedHeight());

        if (viewWidth <= viewHeight) {
            this.traslationX = 0;
            this.traslationY = (maxDistViewSize - minDistViewSize) / 2;
        } else {
            this.traslationX = (maxDistViewSize - minDistViewSize) / 2;
            this.traslationY = 0;
        }
        // Adding artificial padding, depending on line width
        wheelBoundsRectF = new Rect(0 + itemsLineWidth, 0 + itemsLineWidth, minDistViewSize - itemsLineWidth, minDistViewSize - itemsLineWidth);

        return true;
    }

    @Override
    public void arrange(int left, int top, int width, int height) {
        super.arrange(left, top, width, height);
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        canvas.translate(traslationX, traslationY);
        if (circleBackgroundPaint != null)
            canvas.drawCircle(wheelBoundsRectF.getCenterX(), wheelBoundsRectF.getCenterY(), wheelBoundsRectF.getWidth() / 2 - itemsLineWidth, circleBackgroundPaint);
        RectFloat rectFloat = new RectFloat(wheelBoundsRectF);
        Arc arc = new Arc();
        arc.setArc(ANGLE_INIT_OFFSET, 360, false);
        canvas.drawArc(rectFloat, arc, innerBackgroundCirclePaint);
        drawIndicatorItems(canvas);
    }

    private void drawIndicatorItems(Canvas canvas) {
        if (wheelIndicatorItems.size() > 0) {
            for (int i = wheelIndicatorItems.size() - 1; i >= 0; i--) { // Iterate backward to overlap larger items
                draw(wheelIndicatorItems.get(i), wheelBoundsRectF, wheelItemsAngles.get(i), canvas);
            }
        }
    }

    private void draw(WheelIndicatorItem indicatorItem, Rect surfaceRectF, float angle, Canvas canvas) {
        itemArcPaint.setColor(new Color(indicatorItem.getColor()));
        itemEndPointsPaint.setColor(new Color(indicatorItem.getColor()));
        // Draw arc
        RectFloat rectFloat = new RectFloat(surfaceRectF);
        Arc arc = new Arc();
        arc.setArc(ANGLE_INIT_OFFSET, angle, false);
        canvas.drawArc(rectFloat, arc, itemArcPaint);
        // Draw top circle
        canvas.drawCircle(minDistViewSize / 2, 0 + itemsLineWidth, itemsLineWidth, itemEndPointsPaint);
        int topPosition = minDistViewSize / 2 - itemsLineWidth;
        // Draw end circle
        canvas.drawCircle(
                (float) (Math.cos(Math.toRadians(angle + ANGLE_INIT_OFFSET)) * topPosition + topPosition + itemsLineWidth),
                (float) (Math.sin(Math.toRadians((angle + ANGLE_INIT_OFFSET))) * topPosition + topPosition + itemsLineWidth), itemsLineWidth, itemEndPointsPaint);
    }
}
